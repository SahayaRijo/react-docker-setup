# Stage 1: Build the React application
FROM node:14 as build
WORKDIR /app

# Copy package.json and package-lock.json to install dependencies
COPY package*.json ./
RUN npm install

# Copy the entire application
COPY . .

# Build the React app
RUN npm run build

# Stage 2: Create a lightweight production image
FROM nginx:alpine
WORKDIR /usr/share/nginx/html

# Copy only the necessary artifacts from the build stage
COPY --from=build /app/build .

# Expose port 80 (the default port for HTTP)
EXPOSE 80

# Start Nginx to serve the production build
CMD ["nginx", "-g", "daemon off;"]
